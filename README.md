# ATM-prototype

### Installation ###

#### Install ####
```
yarn install
```
#### Add dependencies ####
```
yarn install
```

### Usage ###

#### Start in local environment ####
```
yarn start
```
#### Run tests ####
```
yarn test
```

### Commit emojis ###

:sparkles: `:sparkles:` features/code updates <br />
:bug: `:bug:` bug fix <br /> 
:poop: `:poop:` ugly workaround <br />
:toilet: `:toilet:` cleaning/refactor <br />
