import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from '../../utils/themes/default';

export const INPUT_WIDTH = 380;
export const INPUT_HEIGHT = 30;
export const INPUT_MESSAGE_WIDTH = 400;

export const Input = styled.input`
  width: ${INPUT_WIDTH}px;
  height: ${INPUT_HEIGHT}px;
  padding: ${theme.spacing * 2}px;
  color: ${theme.palette.secondary};
  font-size: ${theme.font.body}px;
  text-align: end;
  background: ${theme.palette.grey};
  border-radius: ${theme.borderRadius}px;
  cursor: default;

  :disabled {
    border: none;
  }
`;

export const ErrorMessage = styled.span`
  width: ${INPUT_MESSAGE_WIDTH}px;
  color: ${theme.palette.error};
  font-size: ${theme.font.caption}px;
  font-weight: bold;
`;

const AmountField = ({ value, errorMessage }) => (
  <Fragment>
    <Input role="amount" value={value} disabled required />
    {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
  </Fragment>
);

AmountField.propTypes = {
  value: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
};

export default AmountField;
