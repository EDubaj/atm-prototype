import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { ErrorMessage, Input } from './index';
import theme from '../../utils/themes/default';

test('Input styles', () => {
  const tree = renderer.create(<Input />).toJSON();
  expect(tree).toHaveStyleRule('border', 'none', { modifier: ':disabled' });
});

test('ErrorMessage styles', () => {
  const tree = renderer.create(<ErrorMessage />).toJSON();
  expect(tree).toHaveStyleRule('color', theme.palette.error);
});
