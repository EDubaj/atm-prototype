import React from 'react';
import UserProvider from '../../context-providers/UserProvider';
import DefaultTheme from '../default-theme';
import Container from '../container';
import UserProfile from '../user-profile';
import BalanceUpdate from '../balance-update';

const App = () => (
  <UserProvider>
    <DefaultTheme>
      <Container>
        <UserProfile />
        <BalanceUpdate />
      </Container>
    </DefaultTheme>
  </UserProvider>
);

export default App;
