import React, { useState } from 'react';
import Button from '../button';
import Keyboard from '../keyboard';
import Box from '../box';

export const WITHDRAW_MODE = 'withdraw_mode';
export const DEPOSIT_MODE = 'deposit_mode';

const BalanceUpdate = () => {
  const [openKeyboard, setOpenKeyboard] = useState(false);
  const [mode, setMode] = useState('');
  
  const handleKeyboard = event => {
    setMode(event.target.id);
    setOpenKeyboard(true);
  };
  
  if (openKeyboard) {
    return <Keyboard setOpen={setOpenKeyboard} mode={mode} />;
  } else {
    return (
      <Box>
        <Button onClick={(e) => handleKeyboard(e)} id={WITHDRAW_MODE}>
          Withdraw
        </Button>
        <Button onClick={(e) => handleKeyboard(e)} id={DEPOSIT_MODE}>
          Deposit
        </Button>
      </Box>
    );
  }
};

export default BalanceUpdate;
