import styled from 'styled-components';
import theme from '../../utils/themes/default';

export const BOX_WIDTH = 400;
export const BOX_BORDER_RADIUS = 10;

const Box = styled.div`
  max-width: ${BOX_WIDTH}px;
  height: fit-content;
  margin: auto;
  padding: ${theme.spacing * 4}px;
  background-color: ${theme.palette.white};
  box-shadow: ${theme.boxShadow};
  border-radius: ${BOX_BORDER_RADIUS}px;
`;

export default Box;
