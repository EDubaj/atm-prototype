import renderer from 'react-test-renderer';
import KeyboardButton, { BOX_WIDTH } from './index';
import React from 'react';
import 'jest-styled-components';

test('Box styles', () => {
  const tree = renderer.create(<KeyboardButton />).toJSON();
  expect(tree).toHaveStyleRule('max-width', `${BOX_WIDTH}px`);
  expect(tree).toHaveStyleRule('margin', 'auto');
});
