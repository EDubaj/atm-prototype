import styled from 'styled-components';
import Color from 'color';
import theme from '../../utils/themes/default';

export const BUTTON_WIDTH = 390;

const Button = styled.button`
  width: ${BUTTON_WIDTH}px;
  height: auto;
  margin: ${theme.spacing}px;
  padding: ${theme.spacing * 2}px;
  background: ${props => theme.palette[props.color ? props.color : 'primary']};
  border-radius: ${theme.borderRadius}px;
  border: 0;
  color: ${theme.palette.white};
  font-weight: bold;
  font-size: ${theme.font.body}px;
  text-transform: uppercase;
  outline: 0;
  cursor: pointer;

  :hover,
  :active,
  :focus {
    background: ${props =>
      Color(theme.palette[props.color ? props.color : 'primary'])
        .darken(0.3)
        .string()};
  }
}
`;

export default Button;
