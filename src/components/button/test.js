import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Button from './index';
import theme from '../../utils/themes/default';

test('Button background styles', () => {
  const tree = renderer
    .create(
      <Button color="success">
        test
      </Button>,
    )
    .toJSON();
  expect(tree).toHaveStyleRule('background', theme.palette.success);
});
