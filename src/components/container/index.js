import styled from 'styled-components';
import theme from '../../utils/themes/default';

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  color: ${theme.palette.secondary};
  display: table-cell;
  vertical-align: middle;
`;

export default Container;
