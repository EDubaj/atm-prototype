import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import theme from '../../utils/themes/default';
import Container from './index';

test('Container styles', () => {
  const tree = renderer.create(<Container />).toJSON();
  expect(tree).toHaveStyleRule('height', '100vh');
  expect(tree).toHaveStyleRule('width', '100vw');
  expect(tree).toHaveStyleRule('color', theme.palette.secondary);
  expect(tree).toHaveStyleRule('display', 'table-cell');
  expect(tree).toHaveStyleRule('vertical-align', 'middle');
});
