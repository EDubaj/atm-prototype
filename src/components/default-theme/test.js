import renderer from 'react-test-renderer';
import React from 'react';
import DefaultTheme from './index';

test('DefaultTheme content', () => {
  const tree = renderer
    .create(
      <DefaultTheme>
        <div>test</div>
      </DefaultTheme>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
