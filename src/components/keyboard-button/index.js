import styled from 'styled-components';
import Button from '../button/index';
import theme from '../../utils/themes/default';

const KeyboardButton = styled(Button)`
  width: 32%;
  height: auto;
  margin: 0;
  font-size: ${theme.font.body}px;
  font-weight: bold;
`;

export default KeyboardButton;
