import renderer from 'react-test-renderer';
import KeyboardButton from './index';
import React from 'react';
import 'jest-styled-components';

test('Keyboard button styles', () => {
  const tree = renderer.create(<KeyboardButton />).toJSON();
  expect(tree).toHaveStyleRule('width', '32%');
  expect(tree).toHaveStyleRule('height', 'auto');
  expect(tree).toHaveStyleRule('margin', '0');
});

