import theme from '../../utils/themes/default';
import styled from 'styled-components';

export const KEYBOARD_WIDTH = 400;
export const KEYBOARD_HEIGHT = 400;

const KeyboardWrapper = styled.div`
  width: ${KEYBOARD_WIDTH}px;
  height: ${KEYBOARD_HEIGHT}px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: ${theme.spacing * 5}px;

  button:nth-child(3n) {
    margin-bottom: ${theme.spacing * 2}px;
  }

  button:not(:nth-child(3n)) {
    margin-right: ${theme.spacing * 2}px;
    margin-bottom: ${theme.spacing * 2}px;
  }
`;

export default KeyboardWrapper;
