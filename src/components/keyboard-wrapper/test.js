import renderer from 'react-test-renderer';
import KeyboardButton from './index';
import React from 'react';
import 'jest-styled-components';

test('Keyboard wrapper styles', () => {
  const tree = renderer.create(<KeyboardButton />).toJSON();
  expect(tree).toHaveStyleRule('margin-top', `20px`);
  expect(tree).toHaveStyleRule('margin-bottom', `8px`, { modifier: 'button:nth-child(3n)' });
  expect(tree).toHaveStyleRule('margin-bottom', `8px`, { modifier: 'button:not(:nth-child(3n))' });
  expect(tree).toHaveStyleRule('margin-right', `8px`, { modifier: 'button:not(:nth-child(3n))' });
});
