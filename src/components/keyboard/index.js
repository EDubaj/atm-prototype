import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import AmountField from '../amount-field';
import useKeyboardInput from '../../hooks/useKeyboardInput.js';
import KeyboardButton from '../keyboard-button';
import { CLEAR } from '../../hooks/useKeyboardInput';
import { UserContext } from '../../context-providers/UserProvider';
import { WITHDRAW_MODE } from '../balance-update';
import KeyboardWrapper from '../keyboard-wrapper';
import Box from '../box';

const NUMBER_KEYS = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '0',
  '.',
  '00',
];

const Keyboard = props => {
  const { setOpen, mode } = props;
  const [inputValue, setInputValue] = useState('');
  const [error, setError] = useState('');
  const keyboard = useKeyboardInput(inputValue);
  const { user, updateUser } = useContext(UserContext);
  
  const ERROR_MESSAGES = [
    {
      predicate: value => value > user.balance && mode === WITHDRAW_MODE,
      message:
        'Amount of money to withdraw must be lower than your current balance.',
    },
    {
      predicate: value => value === '',
      message: 'Value cannot be empty',
    },
    {
      predicate: (value) => isNaN(value),
      message: 'Invalid value',
    },
  ];
  
  const addNumberInput = key => keyboard.createEvent(key.toString());
  const clearInput = () => keyboard.createEvent(CLEAR.toString());
  const submitInput = () => {
    const { value } = keyboard;
    const errors = ERROR_MESSAGES.filter(condition =>
      condition.predicate(value),
    ).map(condition => setError(condition.message));
    
    if (errors.length === 0) {
      let balance =
        mode === WITHDRAW_MODE
        ? user.balance - Number(value)
        : user.balance + Number(value);
      updateUser({ balance: balance });
      setOpen(false);
    }
  };
  
  useEffect(() => {
    setInputValue(keyboard.value);
  }, [keyboard.value]);
  
  return (
    <Box>
      <AmountField value={inputValue} errorMessage={error} />
      <KeyboardWrapper>
        {NUMBER_KEYS.map(key => (
          <KeyboardButton
            onClick={() => addNumberInput(key)}
            key={`button-${key}`}
          >
            {key}
          </KeyboardButton>
        ))}
        <KeyboardButton
          onClick={() => setOpen(false)}
          key="button-cancel"
          color="error"
        >
          Cancel
        </KeyboardButton>
        <KeyboardButton
          onClick={() => clearInput()}
          key="button-clear"
          color="neutral"
        >
          Clear
        </KeyboardButton>
        <KeyboardButton
          onClick={() => submitInput()}
          key="button-accept"
          color="success"
        >
          Accept
        </KeyboardButton>
      </KeyboardWrapper>
    </Box>
  );
};

Keyboard.propTypes = {
  setOpen: PropTypes.func.isRequired,
  mode: PropTypes.string.isRequired,
};

export default Keyboard;
