import React, { useContext } from 'react';
import styled from 'styled-components';
import Box from '../box';
import theme from '../../utils/themes/default';
import {UserContext} from '../../context-providers/UserProvider';

export const PROFILE_WRAPPER_HEIGHT = 50;

const ProfileWrapper = styled(Box)`
  height: ${PROFILE_WRAPPER_HEIGHT}px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${theme.spacing * 3}px;
`;

const UserProfile = () => {
  const { user } = useContext(UserContext);
  
  return (
    <ProfileWrapper>
      <h3>{user.username}</h3>
      <h4>Balance: {user.balance} $</h4>
    </ProfileWrapper>
  );
};

export default UserProfile;
