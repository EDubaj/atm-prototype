import React, { useReducer } from 'react';

export const USER_MOCK = {
  id: 1,
  username: 'Test User',
  balance: 2000,
};

const UPDATE_USER = 'update.user';

const userReducer = (state, action) => {
  switch (action.type) {
    case UPDATE_USER:
      return { ...action.data };
    default:
      return state;
  }
};

export const UserContext = React.createContext();

const UserProvider = ({ children }) => {
  const [user, dispatch] = useReducer(userReducer, USER_MOCK);
  
  const updateUser = values => {
    return dispatch({ type: UPDATE_USER, data: Object.assign(user, values) });
  };
  
  return (
    <UserContext.Provider value={{ user, dispatch, updateUser }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
