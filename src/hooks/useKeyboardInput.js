import { useEffect, useRef, useState } from 'react';

export const CLEAR = 'clear';

function useKeyboardInput(initialValue) {
  const [value, setValue] = useState(initialValue);
  const refValue = useRef(initialValue);

  useEffect(() => {
    refValue.current = value;
  }, [value]);

  const pressedKeyHandler = event => {
    const { key } = event;
    if (key === CLEAR) {
      setValue('');
    } else {
      setValue(refValue.current + event.key);
    }
  };

  const createEvent = key =>
    pressedKeyHandler(new KeyboardEvent('keydown', { key }));

  useEffect(() => {
    document.addEventListener('keydown', pressedKeyHandler);

    return function cleanup() {
      document.removeEventListener('keydown', pressedKeyHandler);
    };
  }, []);

  return { createEvent, value };
}

export default useKeyboardInput;
