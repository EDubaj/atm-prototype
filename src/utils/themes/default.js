const theme = {
  palette: {
    primary: '#67A6F7',
    secondary: '#626e7b',
    error: '#fd6a5f',
    neutral: '#ff8c5a',
    success: '#5ed689',
    white: '#FFFFFF',
    grey: '#F5F8FE',
  },
  font: {
    caption: 12,
    body: 18,
  },
  boxShadow: '0 15px 35px 0 rgba(206, 202, 202, 0.05), 0 5px 15px 0 rgba(158, 158, 158, 0.11)',
  spacing: 4,
  borderRadius: 4,
};

export default theme;
